<?php include './partials/header.php';?>

<div class="page single_page">
	<div class="wrapper content">
		<a href="#" class="back_button">Back to title page</a>
		<div class="clear"></div>
		<h2>What we do?</h2>
		<div class="circle_holder">
			<div class="mobile_line"></div>
			<div class="circle"></div>
			<div class="stop stop_1" data-pop="about_1">
				<img src="./media/images/stop_1.png" alt="">
				<div class="text_holder">
					<div class="center">R&D, oil and fiber crops breeding<br/>and original hemp variety</div>
				</div>
			</div>
			<div class="stop stop_2" data-pop="about_2">
				<img src="./media/images/stop_2.png" alt="">
				<div class="text_holder">
					<div class="center">
						Supply of hemp fiber for textile,<br/>
						composite material, construction,<br/>
						pulp & paper industries
					</div>
				</div>
			</div>
			<div class="stop stop_3" data-pop="about_3">
				<img src="./media/images/stop_3.png" alt="">
				<div class="text_holder">
					<div class="center">
						Farmer’s support for<br/>hemp harvesting
					</div>
				</div>
			</div>
			<div class="stop stop_4" data-pop="about_4">
				<img src="./media/images/stop_4.png" alt="">
				<div class="text_holder">
					<div class="center">
						Factory processing: decortification,<br/>
						refining &  opening, shieves cleaning<br/>
						and sorting
					</div>
				</div>
			</div>
			<div class="circle_text">
				<div class="center">
					Lorem Ipsum<br/>is simply text
				</div>
			</div>
		</div>
		<h2>About us</h2>
		<div class="simple_text">
			Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.Consectetur, adipisci velit, ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? lit, ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? 
		</div>
		<a href="#" class="back_button">Back to title page</a>
	</div>
</div>

<?php include './partials/footer.php';?>