				<footer>
					<div class="wrapper">
						<h2>Contacts</h2>
						<div class="link_groups">
							<div class="links">
								<div class="name">Office:</div>
								<a href="https://www.google.lt/maps/place/Ozo+g.+12,+Vilnius+08200/@54.7137969,25.2749202,17z/data=!3m1!4b1!4m5!3m4!1s0x46dd914d8cf327a5:0xbddaf5459bda7fd8!8m2!3d54.7137969!4d25.2771089?hl=lt" target="_blank">Adress: Ozo g. 12A, Vilnius Lithuania</a>
								<div class="clear"></div>
								<a href="tel:+37052394800">Phone: +370 5 2394800</a>
								<a href="mailto:info@naturalfiber.eu">E-mail: info@naturalfiber.eu</a>
							</div>
							<div class="links">
								<div class="name">Factory:</div>
								<a href="https://www.google.lt/maps/place/Ozo+g.+12,+Vilnius+08200/@54.7137969,25.2749202,17z/data=!3m1!4b1!4m5!3m4!1s0x46dd914d8cf327a5:0xbddaf5459bda7fd8!8m2!3d54.7137969!4d25.2771089?hl=lt" target="_blank">Adress: Ozo g. 12A, Vilnius Lithuania</a>
								<div class="clear"></div>
								<a href="tel:+37052394800">Phone: +370 5 2394800</a>
								<a href="mailto:info@naturalfiber.eu">E-mail: info@naturalfiber.eu</a>
							</div>
						</div>
					</div>
					<div id="map"></div>
					<div class="copyrights">
						<div class="wrapper">
							© 2018 NATURALUS PLUOŠTAS, UAB.  All RIGHTS RESERVED
						</div>
					</div>
				</footer>
			</div>
		</main>

		<?php include 'popups.php';?>
		
		<script>
			function initMap() {
				var center = {lat: 54.7137969, lng: 25.27710890000003};
				var map = new google.maps.Map(document.getElementById('map'), {
					center: center,
					zoom: 4,
					scrollwheel: false,
					gestureHandling: 'cooperative',
					styles: [
					    {
					        "featureType": "water",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#e9e9e9"
					            },
					            {
					                "lightness": 17
					            }
					        ]
					    },
					    {
					        "featureType": "landscape",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#f5f5f5"
					            },
					            {
					                "lightness": 20
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "#ffffff"
					            },
					            {
					                "lightness": 17
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "elementType": "geometry.stroke",
					        "stylers": [
					            {
					                "color": "#ffffff"
					            },
					            {
					                "lightness": 29
					            },
					            {
					                "weight": 0.2
					            }
					        ]
					    },
					    {
					        "featureType": "road.arterial",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#ffffff"
					            },
					            {
					                "lightness": 18
					            }
					        ]
					    },
					    {
					        "featureType": "road.local",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#ffffff"
					            },
					            {
					                "lightness": 16
					            }
					        ]
					    },
					    {
					        "featureType": "poi",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#f5f5f5"
					            },
					            {
					                "lightness": 21
					            }
					        ]
					    },
					    {
					        "featureType": "poi.park",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#dedede"
					            },
					            {
					                "lightness": 21
					            }
					        ]
					    },
					    {
					        "elementType": "labels.text.stroke",
					        "stylers": [
					            {
					                "visibility": "on"
					            },
					            {
					                "color": "#ffffff"
					            },
					            {
					                "lightness": 16
					            }
					        ]
					    },
					    {
					        "elementType": "labels.text.fill",
					        "stylers": [
					            {
					                "saturation": 36
					            },
					            {
					                "color": "#333333"
					            },
					            {
					                "lightness": 40
					            }
					        ]
					    },
					    {
					        "elementType": "labels.icon",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "transit",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#f2f2f2"
					            },
					            {
					                "lightness": 19
					            }
					        ]
					    },
					    {
					        "featureType": "administrative",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "#fefefe"
					            },
					            {
					                "lightness": 20
					            }
					        ]
					    },
					    {
					        "featureType": "administrative",
					        "elementType": "geometry.stroke",
					        "stylers": [
					            {
					                "color": "#fefefe"
					            },
					            {
					                "lightness": 17
					            },
					            {
					                "weight": 1.2
					            }
					        ]
					    }
					]
				});
				var marker = new google.maps.Marker({
					position: center,
					map: map
		        });
			}
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCruTDL_WjrDJwzCKX_mgZ4N1caGStbj0c&callback=initMap"
		    async defer></script>
		<script async defer type="text/javascript" src="js/script.js"></script>

<!-- 		Auselė fronto peržiūrai - ištrinti! -->

<style>
	#mmm{
	    position:fixed;
	    width:300px;
	    right:-310px;
	    width:300px;
	    top:0;
	    background: #1a1b1f;
	    z-index:200;
	    padding:15px;
	    top:100px;
	}
	#mmm:hover{
	    right:0;
	}
	#mmm a{
	    text-decoration:none;
	    color:#fff;
	    font-size:12px;
	    display:block;
	}
	#ooo{
	    width:44px;
	    height:34px;
	    position:absolute;
	    top:0;
	    left:-44px;
	    background: #1a1b1f;
	    border: 0px solid #000000;
	    cursor: pointer;
	}
</style>

<div id="mmm">
    <div id="ooo"></div>
    <?php
    $files = scandir(getcwd());
    foreach($files as $k => $file){
        if (strpos($file,'.php') !== false)
            echo '<a href="'.$file.'">'.$file.'</a>';
    }
    ?>
</div>

<!-- 		Auselės pabaiga -->

	</body>
</html>