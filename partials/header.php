<!DOCTYPE HTML>
<html>
	<head>
		<title>Natural fiber</title>	
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
		<!-- 		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> -->
	</head>
	<body>
		<header class="white">
			<div class="wrapper">
				<a href="#" class="logo">
					<span class="white"></span>
					<span class="color"></span>
				</a>
				<div class="right">
					<div class="menu">
						<a class="active" href="#">About us</a>
						<a href="#">Products</a>
						<a href="#">Myth about fibre hemp</a>
						<a href="#">FAQ</a>
						<a href="#">News</a>
						<a href="#">Contacts</a>
					</div>
					<div class="langs">
						<span class="current">EN</span>
						<div class="list">
							<a href="#">LT</a>
							<a href="#">RU</a>
						</div>
					</div>
					<a href="https://www.linkedin.com/company/27248658/" target="_blank" class="linkedin"></a>
					<div class="burger">
						<div class="plank"></div>
						<div class="plank"></div>
						<div class="plank"></div>
						<div class="plank"></div>
					</div>
				</div>
			</div>
		</header>
		<div class="mobile_menu">
			<div class="holder">
				<a class="active" href="#">About us</a>
				<a href="#">Products</a>
				<a href="#">Myth about fibre hemp</a>
				<a href="#">FAQ</a>
				<a href="#">News</a>
				<a href="#">Contacts</a>
				<a href="https://www.linkedin.com/company/27248658/" target="_blank" class="linkedin"><span>Linkedin</span></a>
			</div>
		</div>
		<main>
			<div id="scroller">