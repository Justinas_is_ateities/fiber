<?php include './partials/header.php';?>

<div class="page product_page">
	<div class="wrapper">
		<a href="#" class="back_button">Back to title page</a>
		<div class="clear"></div>
		<h1>Product name 1</h1>
		<div class="product_text_block">
			<div class="left">
				<img src="./media/images/product_1.jpg" alt="product_1">
			</div>
			<div class="right">
				Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.Consectetur, adipisci velit, ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? lit, ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? 
				<div class="downloads">
					<a href="#" class="download_button">Produkto specifikacija parsisiuntimui</a>
					<div class="clear"></div>
					<a href="#" class="download_button">Produkto kainininkas</a>
				</div>
				<div class="industries">
					Where this product is used?
					<div class="industries_holder">
						<div class="industry icon_gyvulininkyste">
							Kraikas
							<div class="tooltip">
								<div class="holder">
									<ul>
										<li>Lorem ipsum olor</li>
										<li>Lorem ipsum olor</li>
										<li>Lorem ipsum olor lorem ipsum olor</li>
										<li>Lorem ipsum olor</li>
										<li>Lorem ipsum olor</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="industry icon_kompozicines_medziagos">
							Kompozicinės medžiagos
							<div class="tooltip">
								<ul>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
								</ul>
							</div>
						</div>
						<div class="industry icon_popierius">
							Popierius
							<div class="tooltip">
								<ul>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
								</ul>
							</div>
						</div>
						<div class="industry icon_sodininkyste">
							Sodininkystė
							<div class="tooltip">
								<ul>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
								</ul>
							</div>
						</div>
						<div class="industry icon_statybos">
							Statybos
							<div class="tooltip">
								<ul>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
								</ul>
							</div>
						</div>
						<div class="industry icon_tekstile">
							Tekstilė
							<div class="tooltip">
								<ul>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
									<li>Lorem ipsum olor</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="contact_form_holder">
			<div class="left">
				<div class="info">
					<div class="img" style="background-image: url('./media/images/face.jpg')"></div>
					<div class="texts">
						<div class="name">Johns Hempas</div>
						<div class="mail">jonas@naturalfiber.eu</div>
						<div class="number">+370 6 78 158</div>
					</div>
				</div>
			</div>
			<div class="right">
<!-- 				Javascripto submit funkcija yra script.js faile -->
				<form id="contact_form">
					<input type="text" name="name" placeholder="Jūsų vardas">
					<input type="text" name="company" placeholder="Įmonės pavadinimas">
					<input type="email" name="email" placeholder="El. paštas">
					<input type="number" name="phone" placeholder="Telefono nr.">
					<textarea placeholder="Jūsų žinutė" name="message"></textarea>
					<button type="submit" class="button">Siųsti užklausą</button>
				</form>
			</div>
		</div>
	</div>
</div>

<?php include './partials/footer.php';?>