<?php include './partials/header.php';?>

<div class="page title_page">
	<div class="hero">
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide" style="background-image: url('./media/images/hero.jpg')">
					<div class="wrapper center white">
						<h2>Cultivating tradition of textile hemp</h2>
						<div class="comment">The fiber is one of the most valuable parts of the hemp plant</div>
					</div>
				</div>
				<div class="swiper-slide" style="background-image: url('./media/images/hero.jpg')">
					<div class="wrapper center black">
						<h2>Cultivating tradition of textile hemp</h2>
						<div class="comment">The fiber is one of the most valuable parts of the hemp plant</div>
					</div>
				</div>
				<div class="swiper-slide" style="background-image: url('./media/images/hero.jpg')">
					<div class="wrapper center white">
						<h2>Cultivating tradition of textile hemp</h2>
						<div class="comment">The fiber is one of the most valuable parts of the hemp plant</div>
					</div>
				</div>
			</div>
			<div class="swiper-button-next white"></div>
			<div class="swiper-button-prev white"></div>
		</div>
		<div class="discover">
			Discover more
		</div>
	</div>
	<section class="products">
		<div class="wrapper">
			<h2>Products</h2>
			<div class="text">We stock the largest supply of degummed hemp fiber, which can be used as a stuffing material for hypo-allergenic persons, hemp wick vaping or anything else you can think of.</div>
			<div class="products_holder">
				<a href="#" class="product">
					<span class="ratio">
						<span class="img" style="background-image: url('./media/images/product_1.jpg')"></span>
					</span>
					<span class="name">Product name 1</span>
				</a>
				<a href="#" class="product">
					<span class="ratio">
						<span class="img" style="background-image: url('./media/images/product_2.jpg')"></span>
					</span>
					<span class="name">Product name 2</span>
				</a>
				<a href="#" class="product">
					<span class="ratio">
						<span class="img" style="background-image: url('./media/images/product_1.jpg')"></span>
					</span>
					<span class="name">Product name 1</span>
				</a>
				<a href="#" class="product">
					<span class="ratio">
						<span class="img" style="background-image: url('./media/images/product_2.jpg')"></span>
					</span>
					<span class="name">Product name 2</span>
				</a>
				<a href="#" class="product">
					<span class="ratio">
						<span class="img" style="background-image: url('./media/images/product_1.jpg')"></span>
					</span>
					<span class="name">Product name 1</span>
				</a>
			</div>
		</div>
	</section>
	<section class="about">
		<div class="wrapper">
			<h2>About company</h2>
			<div class="info_block">
				<div class="left">
					<img src="./media/images/stock.png" alt="stock">
				</div>
				<div class="right">
					<div class="holder">
						We uphold the cultivation tradition of textile hemp. We are the company based in Lithuania – a country in the Central Europe with a long history of cultivation of textile hemp – processing and supplying this raw material. Hemp is said to have been one of the first crops in Lithuania. The remains of hemp seeds and products that have been found go as far back as 3,000 BC. For many hemp was an important raw material as it allowed to produce a robust material that could be used in shipping and everyday activities. Historically, hemp cultivation and processing is an integral part of the Lithuanian farming culture, just like flax. Unfortunately, after the WWII, the hemp fibre has been almost completely edged out by flax and artificial fibres.<br/><br/>
						The current global trend reintroducing the hemp was made possible by its environmentally friendly properties, simple cultivation, excellent qualities and wide application possibilities. The last couple of years have been the time of reinstatement of hemp. This is an excellent opportunity for this crop, greatly cherished by Lithuanians, to thrive once again, especially because due to its superb quality, today the Lithuanian hemp is popular and valued around the world.
						<div class="clear"></div>
						<a href="#" class="button more">Read more</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="industries">
		<div class="wrapper">
			<h2>Industries using fiber hemp</h2>
			<div class="text">
				The fiber is one of the most valuable parts of the hemp plant
			</div>
			<div class="industries_holder">
				<div class="industry icon_gyvulininkyste">
					Kraikas
					<div class="tooltip">
						<div class="holder">
							<ul>
								<li>Lorem ipsum olor</li>
								<li>Lorem ipsum olor</li>
								<li>Lorem ipsum olor lorem ipsum olor</li>
								<li>Lorem ipsum olor</li>
								<li>Lorem ipsum olor</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="industry icon_kompozicines_medziagos">
					Kompozicinės medžiagos
					<div class="tooltip">
						<div class="holder">

						</div>
					</div>
				</div>
				<div class="industry icon_popierius">Popierius</div>
				<div class="industry icon_sodininkyste">Sodininkystė</div>
				<div class="industry icon_statybos">Statybos</div>
				<div class="industry icon_tekstile">Tekstilė</div>
				<div class="industry icon_household">Household</div>
				<div class="industry icon_industry">Industry</div>
			</div>
		</div>
	</section>
	<section class="knowing">
		<div class="wrapper">
			<h2>Worth knowing</h2>
			<div class="tabs">
				<div class="questions">
					<div class="tab_head active" data-tab="1">Frequently asked Questions</div>
					<span>/</span>
					<div class="tab_head" data-tab="2">Myths</div>
				</div>
				<div class="tab" data-tab="1">
					<div data-name="faq_1" class="faq">What are the special cultivation conditions?</div>
					<div data-name="faq_2" class="faq">What are the special cultivation conditions?</div>
					<div data-name="faq_3" class="faq">What is fibre hemp most commonly used for?</div>
					<div data-name="faq_4" class="faq">dfgdfg</div>
					<div data-name="faq_5" class="faq">dfg</div>
					<div data-name="faq_6" class="faq">dfgdfg</div>
				</div>
				<div class="tab" data-tab="2">
					<div data-name="myth_1" class="faq">What are the special cultivation conditions?</div>
					<div data-name="myth_2" class="faq">What are the special cultivation conditions?</div>
					<div data-name="myth_3" class="faq">What is fibre hemp most commonly used for?</div>
				</div>
			</div>
		</div>
	</section>
	<section class="news">
		<div class="wrapper">
			<h2>News</h2>
			<div class="text">
				Let’s try to understand fiber hemp industry better
			</div>
			<div class="news_holder">
				<a href="#" class="new">
					<span class="img" style="background-image: url('./media/images/product_1.jpg')"></span>
					<span class="excerpt">
						<span class="center">Launch of construction of the hemp fibre processing plant</span>
					</span>
				</a>
				<a href="#" class="new">
					<span class="img" style="background-image: url('./media/images/product_2.jpg')"></span>
					<span class="excerpt">
						<span class="center">Launch of construction of the hemp fibre processing plant</span>
					</span>
				</a>
				<a href="#" class="new">
					<span class="img" style="background-image: url('./media/images/product_1.jpg')"></span>
					<span class="excerpt">
						<span class="center">Launch of construction of the hemp fibre processing plant</span>
					</span>
				</a>
				<a href="#" class="new">
					<span class="img" style="background-image: url('./media/images/product_2.jpg')"></span>
					<span class="excerpt">
						<span class="center">Launch of construction of the hemp fibre processing plant</span>
					</span>
				</a>
				<a href="#" class="new">
					<span class="img" style="background-image: url('./media/images/product_1.jpg')"></span>
					<span class="excerpt">
						<span class="center">Launch of construction of the hemp fibre processing plant</span>
					</span>
				</a>
			</div>
			<div class="button more">More news</div>
		</div>
	</section>
	<section class="work">
		<div class="wrapper">
			<h2>Nori dirbti pas mus?</h2>
			<div class="text">
				Išsakyk savo lūkesčius - parinksime darbo pasiūlymą individualiai Tau!<br/>
				Savo gyvenimo aprašymą siųsk <a href="mailto:cv@naturalfiber.lt">cv@naturalfiber.lt</a>
			</div>
		</div>
	</section>
</div>

<?php include './partials/footer.php';?>